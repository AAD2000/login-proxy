package ru.odd.login.proxy.filters;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import java.security.Principal;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.stereotype.Component;

@Component
public class QueryParamFilter extends ZuulFilter {

  @Override
  public boolean shouldFilter() {
    return true;
  }

  @Override
  public Object run() throws ZuulException {
    final RequestContext currentContext = RequestContext.getCurrentContext();

    final OAuth2AuthenticationToken userPrincipal = (OAuth2AuthenticationToken) currentContext.getRequest().getUserPrincipal();

    final String email = userPrincipal.getPrincipal().getAttribute("email").toString();

    if(currentContext.getRequestQueryParams() == null){
      currentContext.setRequestQueryParams(new LinkedHashMap<>());
    }

    final Map<String, List<String>> requestQueryParams = currentContext.getRequestQueryParams();

    if(requestQueryParams.get("owner") == null) {
      requestQueryParams.put("owner", Collections.singletonList(email));
    } else {
      requestQueryParams.replace("owner", Collections.singletonList(email));
    }

    return null;
  }

  @Override
  public String filterType() {
    return "pre";
  }

  @Override
  public int filterOrder() {
    return 1;
  }
}
