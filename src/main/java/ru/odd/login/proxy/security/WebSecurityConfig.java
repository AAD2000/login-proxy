package ru.odd.login.proxy.security;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.http.HttpHeaders;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.Http403ForbiddenEntryPoint;
import org.springframework.web.filter.GenericFilterBean;

@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

  protected void configure(HttpSecurity http) throws Exception {
    http
//        .addFilterAfter(new SessionCookieFilter(), SessionCookieFilter.class)
        .exceptionHandling().authenticationEntryPoint(new Http403ForbiddenEntryPoint()).and()
        .authorizeRequests(ar -> ar.anyRequest().authenticated())
        .oauth2Login().defaultSuccessUrl("https://odd-frontend.herokuapp.com/databases").and()
        .logout().logoutSuccessUrl("http://20.81.9.128:8080/auth/realms/odd/protocol/openid-connect/logout?redirect_uri=https%3A%2F%2Fodd-frontend.herokuapp.com%2F").and()
        .cors().and()
        .csrf().disable();
  }

//  public static class SessionCookieFilter extends GenericFilterBean {
//
//    private final String SESSION_COOKIE_NAME = "JSESSIONID";
//    private final String SAME_SITE_ATTRIBUTE_VALUES = ";HttpOnly;Secure;SameSite=None";
//
//    @Override
//    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
//        throws IOException, ServletException {
//      HttpServletRequest req = (HttpServletRequest) request;
//      HttpServletResponse resp = (HttpServletResponse) response;
//      String requestUrl = req.getRequestURL().toString();
//      Cookie[] cookies = ((HttpServletRequest) request).getCookies();
//
//      if (cookies != null && cookies.length > 0) {
//        List<Cookie> cookieList = Arrays.asList(cookies);
//        Cookie sessionCookie = cookieList.stream()
//            .filter(cookie -> SESSION_COOKIE_NAME.equals(cookie.getName())).findFirst()
//            .orElse(null);
//        if (sessionCookie != null) {
//          resp.setHeader(
//              HttpHeaders.SET_COOKIE, sessionCookie.getName() + "=" + sessionCookie.getValue()
//                  + SAME_SITE_ATTRIBUTE_VALUES);
//        }
//
//      }
//      chain.doFilter(request, response);
//    }
//  }

}