package ru.odd.login.proxy.controllers;

import java.security.Principal;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(value = "https://odd-frontend.herokuapp.com", allowCredentials = "true")
@RestController
@RequestMapping("/userinfo")
public class UserInfoController {

  @GetMapping()
  public Principal userInfo(Principal principal) {
    return principal;
  }
}
